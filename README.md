# Test Master-Slave Communications

This is a simple application to test mailbox communications. The
master generates 6 values, some of them changing, and loads them into
mailboxes 0 through 5 which are assigned handshake protocol group
D. The slave picks up these values and displays them on the PIC32
serial terminal.

After displaying, slave waits a while blinking the orange LED to give
an opportunity to see the result.

The slave then adds 0x4000 to the SI1MBX1D value and returns it to the
master over the FIFO. The master then uses this value in MSI1MBX0D
instead of 0xc00 on the next cycle.

Each time it sends data, the master toggles the blue LED.
