/*! \file  testCH02S1.h
 *
 *  \brief Constants and function prototypes for testCH02S1
 *
 *  \author jjmcd
 *  \date July 15, 2018, 3:08 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TESTCH02S1_H
#define	TESTCH02S1_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define LED_ORANGE _LATB5
#define SNORE_COUNT 1000000

#ifdef OLD
/*! Initialize - set mode to portrait */
#define TFTPORTRAIT         0x00
/*! Initialize - set mode to landscape */
#define TFTLANDSCAPE        0x01

/*! Set Font - Small font */
#define FONTSMALL           0x01
/*! Set Font - Big font */
#define FONTBIG             0x02
/*! Set Font - Seven segment number-only font */
#define FONTSEVENSEGNUM     0x03
/*! Set Font - Deja Vu Sans */
#define FONTDJS             0x04
/*! Set Font - Deja Vu Sans Bold */
#define FONTDJSB            0x05
/*! Set Font - Smallbold */
#define FONTBOLDSMALL       0x06
/*! PrintInt - Left justified, decimal */
#define TFTPRINTINTPLAIN    0x0000
/*! PrintInt - Left justified, hexadecimal*/
#define TFTPRINTINTHEX      0x0001
/*! PrintInt - Right justify */
#define TFTPRINTINTRIGHT    0x0002
/*! PrintInt - Zero fill */
#define TFTPRINTINTZERO     0x0004

/*! Initialize TFT */
#define TFTINIT             0x01
/*! Set color RGB */
#define TFTSETCOLOR         0x02
/*! Set Background Color RGB */
#define TFTSETBACKCOLOR     0x03
/*! Set color 16-bit */
#define TFTSETCOLORX        0x04
/*! Set background color 16-bit */
#define TFTSETBACKCOLORX    0x05
/*! Clear the screen */
#define TFTCLEAR            0x06
/*! Print teletype mode */
#define TFTPRINTTT          0x07
/*! Draw a rectangle */
#define TFTRECT             0x08
/*! Clear a rectangle */
#define TFTCLEARRECT        0x09
/*! Draw a pixel */
#define TFTPIXEL            0x0A
/*! Draw a line */
#define TFTLINE             0x0B
/*! Set the font */
#define TFTSETFONT          0x0C
/*! Print a character */
#define TFTPRINTCHAR        0x0D
/*! Draw a rounded rectangle */
#define TFTROUNDRECT        0x0E
/*! Draw a filled rounded rectangle */
#define TFTFILLROUNDRECT    0x0F
/*! Draw a circle */
#define TFTCIRCLE           0x10
/*! Draw a filled circle */
#define TFTFILLCIRCLE       0x11
/*! Draw a filled rectangle */
#define TFTFILLRECT         0x12
/*! Print a number with a decimal */
#define TFTPRINTDEC         0x13
/*! Print a number with a decimal right justified */
#define TFTPRINTDECRIGHT    0x14
/*! Print an integer */
#define TFTPRINTINT         0x15
/*! Fill screen starting at center */
#define TFTTRANSITIONOPEN   0x16
/*! Fill screen starting at edges */
#define TFTTRANSITIONCLOSE  0x17
/*! Wipe a new color on to the screen  */
#define TFTTRANSITIONWIPE   0x18
/*! Initialize a new graph */
#define TFTGINIT            0x19
/*! Ad a point to a simple graph */
#define TFTGADDPT           0x1a
/*! Render the graph frame and annotation */
#define TFTGEXPOSE          0x1b
/*! Set colors to be used for the graph */
#define TFTGCOLORS          0x1c
/* Set the X and Y ranges for the graph */
#define TFTGRANGE           0x1d
/*! Set the graph title */
#define TFTGTITLE           0x1e
/*! Set the X-axis label for the graph */
#define TFTGXLABEL          0x1f
/*! Set the Y-axis label for the graph */
#define TFTGYLABEL          0x20
/*! Turn off the graph cursor */
#define TFTGNOCURSOR        0x21
/*! Sop erasing previous graph trace */
#define TFTGNOERASE         0x22
/*! Show the currenc software version and compile date */
#define TFTSOFTWAREID       0x23
/*! Clear a portion of the screen */
#define TFTERASERECT        0x24
/*! Halt with error display */
#define TFTFATALERROR       0x25
/*! Initialize the touch system */
#define TFTTOUCHINIT        0x26  // 2
/*! Query touch data */
#define TFTTOUCHQUERY       0x27  // 2 r 4
/*! Update touch calibration */
#define TFTTOUCHCAL         0x28  // 17
/*! Make a button, simplest version */
#define TFTBUTMAKE1         0x29  // 10
/*! Query button state */
#define TFTBUTSTATE         0x2a  // 2 r 1
/*! Set the button state */
#define TFTBUTSETSTATE      0x2b  // 2
/*! Set the button visibility */
#define TFTBUTSETVIS        0x2c  // 2
/*! Set button active/inactive */
#define TFTBUTSETACT        0x2d  // 2
/*! Make a button, medium complexity */
#define TFTBUTMAKE2         0x2e
/*! Make a button, full control */
#define TFTBUTMAKE3         0x2f
/*! Control pin by button */
#define TFTBUTPIN           0x30
/*! Control button by pin */
#define TFTBUTBYPIN         0x31
/*! Print a string, teletype style */
#define TFTPUTSTT           0x82
#endif

//void serialPuts( char * );
//void serialPutch( char );
void snore( long );
void initializeSerialPort( void );
void initOscillator(void);

//void TFTclear( void );
//void TFTcolor(char,char,char);
//void TFTputsTT( char * );


#ifdef	__cplusplus
}
#endif

#endif	/* TESTCH02S1_H */

