/*! \file  initDisplay.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date July 22, 2018, 2:33 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "testCH02S1.h"

#define TFTLANDSCAPE        0x01
#define TFTINIT             0x01
#define TFTPRINTTT          0x07
#define TFTPUTSTT           0x82

#define TFTinit(fmt) {  serialPutch(TFTINIT);  serialPutch(fmt); idleSync(); }

void TFTputsTT( char *p )
{
  long i;
  serialPutch(TFTPUTSTT);
  serialPuts(p);
  serialPutch(0);
  for (i = 0; i < 1000000; i++)
    ;
}

/*! initDisplay - */

/*!
 *
 */
void initDisplay(void)
{
  TFTinit(TFTLANDSCAPE);

}
