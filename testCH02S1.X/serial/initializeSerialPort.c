/*! \file  initializeSerialPort.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date July 27, 2018, 2:35 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../../../include/graphicsHostLibrary.h"
#include "../../../include/colors.h"
#include "../testCH02S1.h"

/****** Baud rate generator calculations ********/
/*! Processor frequency */
//#define FCY 78000000
#define FCY 100000000
/*! Desired baud rate                           */
#define BAUDRATE         38400
/*! Value for the baud rate generator           */
#define BRGVAL          ((FCY/BAUDRATE)/16)-1

/*! initializeSerialPort - Initialize the serial port */

/*! Map RC4 (RP52) to the USART U1TX. Set the port up for 8 bits,
 *  no parity, 9600 baud. Set the TXIF so first char can go.
 */
void initializeSerialPort(void)
{
  int i;

  /* Map RC4 (RP52) to U1TX */
  RPOR10bits.RP52R = 0x01;
  /*           fedcba9876543210 */
  U1MODE = 0b1000000000100000;
  /*    UARTEN ^ |||            */
  /*       USIDL ^||            */
  /*         WAKE ^|            */
  /*        RXBIMD ^            */
  /****** FOR TESTING *****************/

  U1BRG = BRGVAL;           /* Set up baud rate generator   */

  snore(100000);
  for ( i=0; i<200; i++ )
    putch(0);
  TFTinit(TFTLANDSCAPE);
  TFTsetFont(FONTDJS);
  TFTsetColorX(HOTPINK);
  // TFTinit()
//  serialPutch(TFTINIT);
//  serialPutch(TFTLANDSCAPE);
//  snore(100000);
  // TFTsetFont
//  serialPutch(TFTSETFONT);
//  serialPutch(FONTDJS);
//  snore(100000);
  // Make it pink
//  serialPutch(TFTSETCOLOR);
//  serialPutch(0xff);
//  serialPutch(0xaf);
//  serialPutch(0xcf);
//  snore(100000);

}
