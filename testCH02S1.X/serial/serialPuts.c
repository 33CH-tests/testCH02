/*! \file  serialPuts.c
 *
 *  \brief Send a null-terminated string to the serial port
 *
 *  \author jjmcd
 *  \date July 22, 2018, 2:28 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../testCH02S1.h"

void snore( long );

/*! serialPutch - Send a character to the serial port */
void serialPutch( char p)
{
  long i;

  while (!IFS0bits.U1TXIF)
    ;
  U1TXREG = p;
  IFS0bits.U1TXIF=0;

  for ( i=0; i<10000; i++ )
    ;
}

/*! serialPuts - Send a null-terminated string to the serial port */
/*!
 *
 */
//void serialPuts( char *p )
//{
  /* If we let the terminal buffer fill, the host can have a  */
  /* hard time managing delays so the user can observe, so we */
  /* waste a bit of time so as not to get ahead of terminal   */
//  serialPutch(TFTPUTSTT);
//  while ( *p )
//    {
//      serialPutch(*p);
//      p++;
//    }
//  serialPutch(0);
//  snore(10000000);
//}
