/*! \file  delay.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 29, 2015, 3:06 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "processorFrequency.h"
extern void __delay32(unsigned long cycles);


/*! delay - */

/*!
 *
 */
void delay(unsigned long l)
{
  __delay32( (unsigned long) (((unsigned long long) l)*(FCY)/(1000ULL)));
}
