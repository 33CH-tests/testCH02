/*! \file  tputs.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 28, 2015, 9:32 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"


/*! tputs - */

/*!
 *
 */
void tputs( char *p )
{
  while( *p )
    putch(*p++);
}
