/*! \file  graphingStrings.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 1, 2015, 1:16 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"

/*! graphingStrings - */

/*!
 *
 */
void graphingStrings(unsigned char opcode, char *p)
{
  int i;

  putch(opcode);
  for ( i=0; i<23; i++ )
    {
      putch( *p );
      p++;
    }
  putch(0);
}
