/*! \file  TFTprintDecRight.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 28, 2015, 7:36 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLibrary.h"

/*! Display an integer on the screen with an inserted decimal point
 *  right-justified
 *
 * \param val int - value to be printed
 * \param dec int - number of places from the right to place the decimal
 * \param x int - horizontal position of the right end of the number on the screen
 * \param y int - vertical position of the number on the screen
 */
void TFTprintDecRight( int val, int dec, int x, int y )
{
    putch( 0x14 );
    sendWord(val);
    sendWord(dec);
    sendWord(x);
    sendWord(y);
}
