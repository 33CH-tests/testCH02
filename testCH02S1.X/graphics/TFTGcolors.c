/*! \file  TFTGcolors.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 1, 2015, 12:47 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"

/*! TFTGcolors - */

/*!
 *
 */
void TFTGcolors(unsigned int ctit,unsigned int cann,unsigned int cxlab,unsigned int cylab,unsigned int cscreen,unsigned int cfield,unsigned int cdata)
{
  putch(TFTGCOLORS);
  sendWord(ctit);
  sendWord(cann);
  sendWord(cxlab);
  sendWord(cylab);
  sendWord(cscreen);
  sendWord(cfield);
  sendWord(cdata);
}

