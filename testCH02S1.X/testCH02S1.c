/*! \file  testCH02S1.c
 *
 *  \brief This file contains the mainline for testCH02S1
 *
 * Test sending to simpleTerminal.X
 *
 *
 *  \author jjmcd
 *  \date July 15, 2018, 3:04 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../include/graphicsHostLibrary.h"
#include "../../include/colors.h"
#include "testCH02S1.h"

/*! Storage for list of variables */
int vars[8];

/*! main - Mainline for testCH02S1 */

/*! Simply display a list of random numbers, appropriately formatted,
 *  on the simple terminal.
 */
int main(void)
{
  int i;
  int updateCounter;
  char szWork[64];

  initOscillator();

  _TRISB5 = 0;
  _TRISC4 = 0;

  initializeSerialPort();

  LED_ORANGE = 0;

  SI1FIFOCSbits.SWFEN = 1;

  TFTclear();

  TFTputsTT("\033[H\033[0J");
  updateCounter = 300;
  while (1)
    {

      // Get the data from master
      while ( !SI1MBXSbits.DTRDYD )   // Wait for data ready
        ;
      vars[0] = SI1MBX0D;
      vars[1] = SI1MBX1D;
      vars[2] = SI1MBX2D;
      vars[3] = SI1MBX3D;
      vars[4] = SI1MBX4D;
      vars[5] = SI1MBX5D;  //SI1MBXSbits.DTRDYD = 0 happens automatically

      if ( updateCounter > 100 )
        {
          updateCounter = 0;
          TFTclear();
      // Make a header for the display
      TFTputsTT("\033[H");
      TFTsetColorX(TURQUOISE);
      TFTputsTT("\033[2;11HMailbox communications\r\n");
      TFTsetBackColorX(NAVY);
      TFTsetColorX(WHITE);
      TFTputsTT("\033[3;11H-----------------------\r\n");
      TFTsetBackColorX(BLACK);
      TFTsetColorX(PINK);
      TFTputsTT("\033[5;29H<--FIFO");
      TFTsetColorX(LAWNGREEN);
      for ( i=0; i<6; i++ )
        {
          sprintf(szWork,"\033[%d;11HSI1MBX%dD = ",i+5,i);
          TFTputsTT(szWork);
        }
        }

      TFTsetColorX(YELLOW);
      // Display the master data
      for ( i=0; i<6; i++ )
        {
          sprintf(szWork,"\033[%d;22H0x%04x\r\n",i+5,vars[i]);
          TFTputsTT(szWork);
        }
      updateCounter++;

      /* Sit around flashing the LED so display can be examined */
      for ( i=0; i<15; i++ )
        {
          LED_ORANGE ^= 1;
          snore(SNORE_COUNT);
        }

      // Put recognizable data into the FIFO. It will be
      // echoed by master into vars[0]]
      vars[0] = 0x4000 + vars[1];
      SWMRFDATA = vars[0];
    }

  return 0;
}