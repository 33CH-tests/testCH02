/*! \file  testCH02.c
 *
 *  \brief This file contains the mainline for testCH02S1
 *
 * #pragma config HSDEN = ON
 * #pragma config MBXHSD = MBX5
 *
 *  \author jjmcd
 *  \date July 15, 2018, 2:40 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <libpic30.h>
#include "testCH02.h"
#include "testCH02S1.h"

/*! main - Mainline for testCH02 */

/*!
 *
 */
int main(void)
{
  unsigned int uTest1,uTest2;
  unsigned int uFIFOvalue;

  /* Disable Watch Dog Timer */
  RCONbits.SWDTEN = 0;

  _TRISC8 = 0;  /* Blue LED */
  _TRISC9 = 0;  /* Pink LED */

  /* Program and start the slave */
  _program_slave(1, 0, testCH02S1);
  _start_slave();

  /* Initialize the oscillator */
  initOscillator();
  MSI1FIFOCSbits.RFEN = 1;

  // Initialize some values to send
  uTest1 = 1;
  uTest2 = 2;
  uFIFOvalue = 0xb0;

  while (1)
    {
      MSI1MBX0D = uFIFOvalue;
      MSI1MBX1D = uTest1;
      MSI1MBX2D = uTest2;
      MSI1MBX3D = uTest1+uTest2;
      MSI1MBX4D = uTest1<<2;
      MSI1MBX5D = 0x5005;   // Automatically sets MSI1MBXSbits.DTRDYD

      // Wait for slave to read data
      while( MSI1MBXSbits.DTRDYD )
        ;

      // Generate some new values
      uTest1++;
      uTest2++;

      // Pick up FIFO data and send back in MSI1MBX0D
      uFIFOvalue = MRSWFDATA;           //  Grab the data

      // Toggle the blue LED each pass
      LED_BLUE ^= 1;
    }

  return 0;
}
