/*! \file  snore.c
 *
 *  \brief Loops for a potentially long time
 *
 *  \author jjmcd
 *  \date July 27, 2018, 7:53 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! snore - Waste time */

/*!
 *
 */
void snore(long howLong)
{
  long i;

  for ( i=0; i<howLong; i++ )
    ;

}
