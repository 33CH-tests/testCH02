/*! \file  testCH02.h
 *
 *  \brief Constants and function prototypes for testCH02
 *
 *  \author jjmcd
 *  \date July 28, 2018, 7:12 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TESTCH02_H
#define	TESTCH02_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define LED_BLUE _LATC8
#define LED_PINK _LATC9

void initOscillator(void);
void snore(long);



#ifdef	__cplusplus
}
#endif

#endif	/* TESTCH02_H */

